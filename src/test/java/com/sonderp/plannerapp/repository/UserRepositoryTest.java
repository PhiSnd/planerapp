package com.sonderp.plannerapp.repository;

import com.sonderp.plannerapp.model.User;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    UserRepository underTest;

    @Test
    void test_findByUsername_ShouldReturnUser() {
        //given
        User givenEntity = new User(
                "Paul",
                "paul@gmail.com",
                "password"
        );
        underTest.save(givenEntity);
        //when
       User expectedEntity =  underTest.findByUsername("Paul").orElseThrow();
        //then
        assertThat(expectedEntity).isEqualTo(givenEntity);
    }

    @Test
    void test_existsByUsername_ShouldReturnTrue() {

        //given
        User givenEntity = new User(
                "Paul",
                "paul@gmail.com",
                "password"
        );
        underTest.save(givenEntity);
        //when
        boolean exists = underTest.existsByUsername("Paul");
        //then

        assertThat(exists).isTrue();
    }

    @Test
    void test_existsByEmail_ShouldReturnTrue() {

        //given
        User givenEntity = new User(
                "Paul",
                "paul@gmail.com",
                "password"
        );
    underTest.save(givenEntity);

    //when
       boolean exists =  underTest.existsByEmail("paul@gmail.com");
    //then
        assertThat(exists).isTrue();
    }

    @Test
    void test_findAll_ShouldReturnAllUser() {

        //given
        User givenEntity1 = new User(
                "Paul",
                "paul@gmail.com",
                "password1"
        );

        User givenEntity2 = new User(
                "Fiona",
                "Fiona@gmail.com",
                "password2"
        );

        User givenEntity3 = new User(
                "Clara",
                "Clara@gmail.com",
                "password3"
        );

        underTest.save(givenEntity1);
        underTest.save(givenEntity2);
        underTest.save(givenEntity3);
        //when
        List<User> entityList = underTest.findAll();

        //then
        assertThat(entityList.size()).isEqualTo(3);
    }

    @Disabled
    @Test
    void deleteByUsername() {

        //given
        User givenEntity = new User(
                "Paul",
                "paul@gmail.com",
                "password"
        );
        underTest.save(givenEntity);

        //when
        underTest.deleteByUsername("Paul");

        //then
        assertThat(underTest.findByUsername("Paul")).isEmpty();
    }
}