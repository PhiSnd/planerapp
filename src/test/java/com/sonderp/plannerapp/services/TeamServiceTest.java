package com.sonderp.plannerapp.services;

import com.sonderp.plannerapp.model.ERole;
import com.sonderp.plannerapp.model.Role;
import com.sonderp.plannerapp.model.Team;
import com.sonderp.plannerapp.model.User;
import com.sonderp.plannerapp.payload.response.MessageResponse;
import com.sonderp.plannerapp.repository.TeamRepository;
import com.sonderp.plannerapp.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class TeamServiceTest {

    @Autowired
    TeamService teamService;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    UserRepository userRepository;




    private Set<Role> createRoleSet(){
        Set<Role> roles = new HashSet<>();

        Role role1 = new Role();
        role1.setName(ERole.ROLE_ADMIN);

        Role role2 = new Role();
        role1.setName(ERole.ROLE_USER);



        roles.add(role1);
        roles.add(role2);


        return roles;
    }


    @Test
    void test_findTeamById_ShouldReturnTeam() {

        //given

        User givenUser1 = User.builder()
                .username("Bart")
                .email("simpsonBart@gmail.com")
                .password("Homer")
                .build();



        User givenUser2 = User.builder()
                .username("Homer")
                .email("Homer@gmail.com")
                .password("Duff")
                .build();

        Set<User> users = new HashSet<>();
        users.add(givenUser1);
        users.add(givenUser2);

        Team givenTeam = Team.builder()
                .id(1L)
                .name("SimpsonsBoys")
                .members(users)
                .build();

        Team createdTeam = teamRepository.save(givenTeam);
        //when
        ResponseEntity<Team> response = teamService.findTeamById(createdTeam.getId());
        //then
        assertEquals(givenTeam.getName(), response.getBody().getName());
    }


        @Test
        void test_deleteById_ShouldNotReturnData(){
            //given
            Set<Role> roles = createRoleSet();
            User givenUser1 = User.builder()
                    .username("Bart")
                    .email("simpsonBart@gmail.com")
                    .password("Homer")
                    .build();

            User givenUser2 = User.builder()
                    .username("Homer")
                    .email("Homer@gmail.com")
                    .password("Duff")
                    .build();
            Set<User> users = new HashSet<>();
            users.add(givenUser1);
            users.add(givenUser2);

            Team givenTeam = Team.builder()
                    .name("SimpsonsBoys")
                    .members(users)
                    .build();

            Team createdTeam = teamRepository.save(givenTeam);
            //when
            teamService.deleteById(createdTeam.getId());
            //then
            assertTrue(teamRepository.findAll().size() == 0);
        }

        @Test
        void test_addMember_shouldReturnTeamWithAddedMember(){
            //given
            Set<Role> roles = createRoleSet();

            Team givenTeam = Team.builder()
                    .id(1L)
                    .name("SimpsonsBoys")
                    .build();
            teamRepository.save(givenTeam);


            User givenUser1 = User.builder()
                    .id(1L)
                    .username("Bart")
                    .email("simpsonBart@gmail.com")
                    .roles(roles)
                    .password("Homer")
                    .build();
            userRepository.save(givenUser1);

            //when
            teamService.addMember(1L,"Bart");
            ResponseEntity<Team> response = teamService.findTeamById(1L);

            //then
            assertTrue(response.getBody().getMembers().size() == 1);

        }

        @Test
        void test_update_ShouldUpdateGivenFields(){

            User givenUser1 = User.builder()
                    .username("Bart")
                    .email("simpsonBart@gmail.com")
                    .password("Homer")
                    .build();

            User givenUser2 = User.builder()
                    .username("Homer")
                    .email("Homer@gmail.com")
                    .password("Duff")
                    .build();
            Set<User> users = new HashSet<>();
            users.add(givenUser1);
            users.add(givenUser2);

            Team givenTeam = Team.builder()
                    .name("SimpsonsBoys")
                    .members(users)
                    .build();
            Team created = teamRepository.save(givenTeam);

            User givenUser3 = User.builder()
                    .username("Lisa")
                    .email("Lisa@gmail.com")
                    .password("Snowball")
                    .build();
            Set<User> updatedUsers = new HashSet<>();
            updatedUsers.add(givenUser1);
            updatedUsers.add(givenUser3);
            Team updateTeam = Team.builder()
                    .name("SimpsonGirls")
                    .members(updatedUsers)
                    .build();
            //when
            ResponseEntity<Team> updatedTeam = teamService.update(created.getId(), updateTeam);

            //then
            assertEquals("SimpsonGirls", updatedTeam.getBody().getName());
        }


}