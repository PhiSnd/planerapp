package com.sonderp.plannerapp.services;

import com.sonderp.plannerapp.model.*;
import com.sonderp.plannerapp.repository.TaskRepository;
import com.sonderp.plannerapp.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional
class TaskServiceTest {


    @Autowired
    TaskService taskService;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    TeamService teamService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    UserDetails userDetails;
    @MockBean
    Authentication auth;

    @MockBean
    SecurityContext securityContext;


   /* @BeforeEach
    void setup(){
        userRepository = Mockito.mock(UserRepository.class);
        userDetails = Mockito.mock(UserDetails.class);
        auth = Mockito.mock(Authentication.class);
        securityContext = Mockito.mock(SecurityContext.class);
    }*/

    @Test
    void test_addToUser() {
    //given

        Set<Role> roles = createRoleSet();
        User user = User.builder()
                .username("Bart")
                .email("simpsonBart@gmail.com")
                .password("Homer")
                .roles(roles)
                .build();

        Mockito.when(userRepository.findByUsername("Bart")).thenReturn(Optional.of(user));

        //Mock UserDetails object
        Mockito.when(userDetails.getUsername()).thenReturn("Bart");

        //Mock Authentication object
        Mockito.when(auth.getPrincipal()).thenReturn(userDetails);

        //Mock SecurityContextHolder
        Mockito.when(securityContext.getAuthentication()).thenReturn(auth);

        SecurityContextHolder.setContext(securityContext);


//-------------------------------

        Task task = Task.builder()
                .created(LocalDate.now())
                .title("TestTask")
                .description("Dies ist eine TestTask")
                .build();

    //when
        ResponseEntity<Task> response = taskService.addToUser(task);

    //then
    assertEquals("TestTask", response.getBody().getTitle());

    }

    @Disabled
    @Test
    void test_addToTeam() {
        Set<Role> roles = createRoleSet();
        User user = User.builder()
                .id(1L)
                .username("Bart")
                .email("simpsonBart@gmail.com")
                .password("Homer")
                .roles(roles)
                .build();

        Mockito.when(userRepository.findByUsername("Bart")).thenReturn(Optional.of(user));

        //Mock UserDetails object
        Mockito.when(userDetails.getUsername()).thenReturn("Bart");

        //Mock Authentication object
        Mockito.when(auth.getPrincipal()).thenReturn(userDetails);

        //Mock SecurityContextHolder
        Mockito.when(securityContext.getAuthentication()).thenReturn(auth);

        SecurityContextHolder.setContext(securityContext);


        //given
        User givenUser1 = User.builder()
                .username("Bart")
                .email("simpsonBart@gmail.com")
                .password("Homer")
                .roles(createRoleSet())
                .build();

        User givenUser2 = User.builder()
                .username("Lisa")
                .email("Lisa@gmail.com")
                .password("Marge")
                .roles(createRoleSet())
                .build();

        Set<User> users = new HashSet<>();
        users.add(givenUser1);
        users.add(givenUser2);

        Task givenTask = Task.builder()
                .title("Müll raus bringen")
                .description("Testbeschreibung")
                .isDone(false)
                .created(LocalDate.now())
                .build();

        Team givenTeam = Team.builder()
                .members(users)
                .build();

        teamService.create(givenTeam);

        //when
       ResponseEntity<Task> response =  taskService.addToTeam(1L ,givenTask);

        //then
       ResponseEntity<Team> resultTeam = teamService.findTeamById(1L);
       Set tasks = resultTeam.getBody().getTasks();

       assertEquals(1, tasks.size());

    }

    @Test
    void getTaskById() {

        User givenUser = User.builder()
                .username("Bart")
                .email("simpsonBart@gmail.com")
                .password("Homer")
                //.roles(createRoleSet())
                .build();

        //given
        Task givenTask = Task.builder()
                .id(1L)
                .title("Müll raus bringen")
                .description("Testbeschreibung")
                .isDone(false)
                .user(givenUser)
                .created(LocalDate.now())
                .build();

        taskRepository.save(givenTask);

        //when
       ResponseEntity<Task> response = taskService.getTaskById(1L);

        //then
        assertEquals(givenTask.getTitle(), response.getBody().getTitle() );

    }

    @Disabled
    @Test
    void findAllTasksByUsername() {
        //given
        Set<Task> tasks = new HashSet<>();


        User givenUser = User.builder()
                .username("Bart")
                .email("simpsonBart@gmail.com")
                .password("Homer")
                .roles(createRoleSet())
                .build();

        Mockito.when(userRepository.findByUsername("Bart")).thenReturn(Optional.of(givenUser));

        Task givenTask1 = Task.builder()
                .title("Müll raus bringen")
                .description("Testbeschreibung")
                .isDone(false)
                .user(givenUser)
                .created(LocalDate.now())
                .build();
        tasks.add(givenTask1);
        givenUser.setTasks(tasks);

       /* Task givenTask2 = Task.builder()
                .title("Fenster putzen")
                .description("Testbeschreibung")
                .isDone(false)
                .user(givenUser)
                .created(LocalDate.now())
                .build();*/

        taskRepository.save(givenTask1);
        //taskRepository.save(givenTask2);

        //when
        ResponseEntity<Set<Task>> response = taskService.findAllTasksByUsername("Bart");

        //then
        assertTrue(response.getBody().size() == 2);
        assertTrue(response.getBody().contains(givenTask1));
       // assertTrue(response.getBody().contains(givenTask2));

    }

    @Test
    void findAllTasksByTeamId() {
    }

    @Test
    void findAllTasksForCurrentUser() {
        //given
        Set<Role> roles = createRoleSet();
        User user = User.builder()
                .id(1L)
                .username("Bart")
                .email("simpsonBart@gmail.com")
                .password("Homer")
                .roles(roles)
                .build();

        Mockito.when(userRepository.findByUsername("Bart")).thenReturn(Optional.of(user));

        //Mock UserDetails object
        Mockito.when(userDetails.getUsername()).thenReturn("Bart");

        //Mock Authentication object
        Mockito.when(auth.getPrincipal()).thenReturn(userDetails);

        //Mock SecurityContextHolder
        Mockito.when(securityContext.getAuthentication()).thenReturn(auth);

        SecurityContextHolder.setContext(securityContext);

        Set<Task> tasks = new HashSet<>();
        Task givenTask1 = Task.builder()
                .id(1L)
                .title("Müll raus bringen")
                .description("Testbeschreibung")
                .isDone(false)
                .user(user)
                .created(LocalDate.now())
                .build();

        Task givenTask2 = Task.builder()
                .id(2L)
                .title("Fenster putzen")
                .description("Testbeschreibung")
                .isDone(false)
                .user(user)
                .created(LocalDate.now())
                .build();
        taskRepository.save(givenTask1);
        taskRepository.save(givenTask2);

        user.setTasks(tasks);

        //when
        ResponseEntity<Set<Task>> response = taskService.findAllTasksForCurrentUser();

        //then
        assertTrue(response.getBody().size() ==2);



    }

    @Test
    void deleteById() {
    }

    @Test
    void getCurrentUser() {
    }

    private Set<Role> createRoleSet(){
        Set<Role> roles = new HashSet<>();

        Role role1 = new Role();
        role1.setName(ERole.ROLE_ADMIN);

        Role role2 = new Role();
        role1.setName(ERole.ROLE_USER);

        roles.add(role1);
        roles.add(role2);


        return roles;
    }

}