package com.sonderp.plannerapp.services;

import com.sonderp.plannerapp.model.ERole;
import com.sonderp.plannerapp.model.Role;
import com.sonderp.plannerapp.model.User;
import com.sonderp.plannerapp.repository.RoleRepository;
import com.sonderp.plannerapp.repository.UserRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.assertj.core.api.AssertionsForClassTypes;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class UserServiceTest {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepo;

    @Autowired
    RoleRepository roleRepo;

    @Disabled
    @Test
    void test_findAll_shouldReturnAllExistingUser() {

        //given
        Role userRole = new Role();
        userRole.setName(ERole.ROLE_USER);

        Set<Role> roles = new HashSet<Role>(){{
            add(userRole);
        }};

        roleRepo.save(userRole);

        User givenUser1 = User.builder()
                .username("Paul")
                .password("password")
                .email("Paul@gmail.com")
                .roles(roles)
                .build();

        User givenUser2 = User.builder()
                .username("Homer")
                .password("password1234")
                .email("homer@gmail.com")
                .roles(roles)
                .build();

        User givenUser3 = User.builder()
                .username("mr.bean")
                .password("passwordTeddy")
                .email("mr.bean@gmail.com")
                .roles(roles)
                .build();

        userRepo.save(givenUser1);
        userRepo.save(givenUser2);
        userRepo.save(givenUser3);

        //when
       ResponseEntity<List<User>> response = userService.findAll();

       //then
        assertTrue(response.getBody().contains(givenUser1));
        assertTrue(response.getBody().contains(givenUser2));
        assertTrue(response.getBody().contains(givenUser3));
    }

    @Disabled
    @Test
    void test_findByUsername_ShouldReturnUser_WhenUserExists() {
        //given
            Role userRole = new Role();
            userRole.setName(ERole.ROLE_USER);

        Set<Role> roles = new HashSet<Role>(){{
            add(userRole);
        }};

        roleRepo.save(userRole);

        User givenUser = User.builder()
                .username("Paul")
                .password("password")
                .email("Paul@gmail.com")
                .roles(roles)
                .build();

        userRepo.save(givenUser);

        //when
        ResponseEntity<User> result = userService.findByUsername("Paul");
       String username =  result.getBody().getUsername();
       String email = result.getBody().getEmail();

       //then

        assertEquals(username,result.getBody().getUsername());
        assertEquals(email,result.getBody().getEmail());

    }

    @Test
    void deleteByUsername() {
    }
}