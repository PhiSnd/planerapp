package com.sonderp.plannerapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Table(name = "team")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;


    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE ,CascadeType.DETACH,CascadeType.REFRESH})
    @JoinTable(name= "teams_users",
            joinColumns = @JoinColumn(name = "team_id"), // "look at the team_id column in teams_users table
            inverseJoinColumns = @JoinColumn(name = "user_id")) //for other side: "look at the user_id column in teams_users table
    private Set<User> members = new HashSet<>();



    @OneToMany(mappedBy = "team", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private Set<Task> tasks = new HashSet<>();

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<User> getMembers() {
        return members;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMembers(Set<User> members) {
        this.members = members;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public void addMember(User user){
        if (members == null){
            members = new HashSet<>();
        }
        if(user.getTeams() == null){
            user.setTeams(new HashSet<>());
        }
        members.add(user);
        user.getTeams().add(this);
    }

    public void removeMember(User user){
        members.remove(user);
        user.getTeams().remove(this);
    }

    public void addTask(Task task){
        if(tasks == null){
            tasks = new HashSet<>();
        }

        tasks.add(task);
        task.setTeam(this);
    }
}
