package com.sonderp.plannerapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;


@NoArgsConstructor
@Entity
@Builder
@AllArgsConstructor
@Table(name = "users", uniqueConstraints = {
                                @UniqueConstraint(columnNames = "username"),
                                @UniqueConstraint(columnNames = "email")
                                })
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    private String username;

    @NotBlank
    @Size(max = 120)
    @Email
    private String email;

    @NotBlank
    @Size(max=120)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.MERGE ,CascadeType.DETACH,CascadeType.REFRESH})
    @JoinTable(name = "user_roles",
               joinColumns = @JoinColumn(name="user_id"), //"look at user_id column in user_roles
               inverseJoinColumns = @JoinColumn(name="role_id"))//for other side: "look at role_id column in user_roles
    private Set<Role> roles = new HashSet<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "members") //members = name of hashSet in other class
    private Set<Team> teams = new HashSet<>();


    @OneToMany (mappedBy = "user",cascade = {CascadeType.ALL}, fetch = FetchType.LAZY) // "user" refers to the property in task class
    private Set<Task> tasks = new HashSet<>();



    public User(String username, String email, String password){
        this.username = username;
        this.email = email;
        this.password = password;
    }

    //methods for bi-directional rel.
    public void addTask(Task task){
        if(tasks ==null){
            tasks = new HashSet<>();
        }
        tasks.add(task);
        task.setUser(this);
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    @JsonIgnore
    public Set<Team> getTeams() {
        return teams;
    }

    @JsonIgnore
    public Set<Task> getTasks() {
        return tasks;
    }
}
