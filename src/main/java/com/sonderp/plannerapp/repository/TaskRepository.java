package com.sonderp.plannerapp.repository;

import com.sonderp.plannerapp.model.Task;
import com.sonderp.plannerapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    Task findTaskByUser(User user);

    Task findTaskById(Long id);

    Set<Task> findAllByUserId(Long id);

    Set<Task> findAllByTeamId(Long id);

    void deleteTaskById(Long id);
}
