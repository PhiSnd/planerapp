package com.sonderp.plannerapp.repository;

import com.sonderp.plannerapp.model.ERole;
import com.sonderp.plannerapp.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {

    Optional<Role> findByName(ERole name);
}
