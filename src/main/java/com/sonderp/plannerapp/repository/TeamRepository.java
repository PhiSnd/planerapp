package com.sonderp.plannerapp.repository;

import com.sonderp.plannerapp.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TeamRepository extends JpaRepository<Team, Long> {

}
