package com.sonderp.plannerapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication //Contains SpringBootConfiguration
public class PlannerAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlannerAppApplication.class, args);
	}

}
