package com.sonderp.plannerapp.payload.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {
    @NotBlank
    private String username;

    @NotBlank
    private String password;

   // public String getUsername() {
        //return username;
    //}

    //public void setUsername(String username) {
       // this.username = username;
   // }

    //public String getPassword() {
      //  return password;
    //}

   // public void setPassword(String password) {
       // this.password = password;
    //}

}
