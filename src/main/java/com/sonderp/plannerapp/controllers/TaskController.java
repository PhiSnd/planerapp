package com.sonderp.plannerapp.controllers;

import com.sonderp.plannerapp.model.Task;
import com.sonderp.plannerapp.payload.response.MessageResponse;
import com.sonderp.plannerapp.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("api/v1/task")
public class TaskController {

    @Autowired
    TaskService taskService;

    //geht
    @PostMapping("/addToUser")
    public ResponseEntity<Task> addToUser(@RequestBody Task task){
       return taskService.addToUser(task);
    }


    //geht
    @PostMapping("/addToTeam/{teamId}")
    public ResponseEntity<Task> addToTeam(@PathVariable("teamId") Long id,@RequestBody Task task){
        return taskService.addToTeam(id, task);
    }

    //geht
    @GetMapping("/getById/{id}")
    public ResponseEntity<Task> getById(@PathVariable("id") Long id){
        return taskService.getTaskById(id);
    }

    //geht
    @GetMapping("/getAllByUsername/{username}")
    public ResponseEntity<Set<Task>> getAllByUsername(@PathVariable("username") String username){
        return taskService.findAllTasksByUsername(username);
    }

    //geht
    @GetMapping("/getAllByTeamId/{teamId}")
    public ResponseEntity<Set<Task>> getAllByTeamId(@PathVariable("teamId") Long teamId){
        return taskService.findAllTasksByTeamId(teamId);
    }

    //geht
    @GetMapping("getAllForCurrentUser")
    public ResponseEntity<Set<Task>> getAllForCurrentUser(){

        return taskService.findAllTasksForCurrentUser();
    }

    //geht
    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<MessageResponse> deleteById(@PathVariable("id") Long id){
        return taskService.deleteById(id);
    }




}
