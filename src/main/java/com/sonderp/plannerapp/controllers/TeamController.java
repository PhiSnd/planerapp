package com.sonderp.plannerapp.controllers;

import com.sonderp.plannerapp.model.Team;
import com.sonderp.plannerapp.payload.response.MessageResponse;
import com.sonderp.plannerapp.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/team")
public class TeamController {

    @Autowired
    TeamService teamService;


    //geht
    @PostMapping("/create")
    public ResponseEntity<Team> create(@RequestBody Team team){
        return teamService.create(team);
    }


    //geht
    @GetMapping("/findById/{id}")
    public ResponseEntity<Team> findById(@PathVariable("id") Long id){
        return teamService.findTeamById(id);
    }


    //geht
    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<MessageResponse> deleteById(@PathVariable("id") Long id){
        return teamService.deleteById(id);
    }

    //geht
    @PutMapping("/updateById/{id}")
    public ResponseEntity<Team> updateById(@PathVariable("id")Long id,@RequestBody Team team){
        return teamService.update(id, team);
    }

    //geht
    @GetMapping("/addMember/{teamId}/{username}")
    public ResponseEntity<MessageResponse> addMember(@PathVariable("teamId")Long teamId,@PathVariable("username") String username){
        return teamService.addMember(teamId, username);
    }


}
