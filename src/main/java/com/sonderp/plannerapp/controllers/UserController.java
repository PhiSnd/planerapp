package com.sonderp.plannerapp.controllers;

import com.sonderp.plannerapp.model.User;
import com.sonderp.plannerapp.payload.response.MessageResponse;
import com.sonderp.plannerapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/getAll")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<User>> findAll() {
        return userService.findAll();
    }

    @DeleteMapping("/delete/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<MessageResponse> deleteByUsername(@PathVariable String username) {
        return userService.deleteByUsername(username);
    }

    @GetMapping("/findByUsername/{username}")
    // @PreAuthorize("hasRole('ADMIN')") -> needed for user search (friends list)
    public ResponseEntity<User> findByUsername(@PathVariable String username) {
        return userService.findByUsername(username);
    }
}

