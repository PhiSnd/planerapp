package com.sonderp.plannerapp.services;

import com.sonderp.plannerapp.exception.Exception.ResourceNotFoundException;
import com.sonderp.plannerapp.model.Team;
import com.sonderp.plannerapp.model.User;
import com.sonderp.plannerapp.payload.response.MessageResponse;
import com.sonderp.plannerapp.repository.TeamRepository;
import com.sonderp.plannerapp.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;


@Service
@Transactional
public class TeamService {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    UserRepository userRepository;

    public ResponseEntity<Team> create(Team team){
        User currentUser = getCurrentUser();
        team.addMember(currentUser);
        teamRepository.save(team);

        return ResponseEntity.status(HttpStatus.CREATED).body(team);
    }

    public ResponseEntity<Team> findTeamById(Long id){
        Team team = teamRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("Error: User Not Found")
        );

        return ResponseEntity.ok(team);
    }

    public ResponseEntity<MessageResponse> deleteById(Long id){
        teamRepository.deleteById(id);

        return ResponseEntity.ok(new MessageResponse("Team deleted successfully"));
    }

    public ResponseEntity<MessageResponse> addMember(Long teamid, String username){
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Error: User not found"));

        Team team = teamRepository.findById(teamid)
                .orElseThrow(() -> new ResourceNotFoundException("Error: Team not found"));

        //check if user is already member of team
        Set<User> users = team.getMembers();
        for (User o: users) {
            if(o.getUsername() == username){
                return ResponseEntity.ok(new MessageResponse("User already Member of this Team"));
            }

        }
        team.addMember(user);
        return ResponseEntity.ok(new MessageResponse("User added successfully"));
    }

    public ResponseEntity<MessageResponse> removeMember(Long teamId, String username){
       Team team =  teamRepository.findById(teamId)
               .orElseThrow(() -> new ResourceNotFoundException("Error: Team was not found"));

       User user = userRepository.findByUsername(username)
                       .orElseThrow(() -> new UsernameNotFoundException("Error: User was not found"));

       team.removeMember(user);

       return ResponseEntity.ok(new MessageResponse("Member was removed successfully"));
    }



    //PutMapping in Controller
    public ResponseEntity<Team> update(Long id, Team newTeam){
       Team currentTeam =  teamRepository.findById(id)
               .orElseThrow(() -> new ResourceNotFoundException("Error: Team doesn't exist"));

        currentTeam.setMembers(newTeam.getMembers());
        currentTeam.setName(newTeam.getName());

       teamRepository.save(currentTeam);

       return ResponseEntity.ok(currentTeam);
    }


    private User getCurrentUser(){

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = userDetails.getUsername();

        User currentUser = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Error: User not found"));

        return currentUser;

    }

}
