package com.sonderp.plannerapp.services;

import com.sonderp.plannerapp.model.User;
import com.sonderp.plannerapp.payload.response.MessageResponse;
import com.sonderp.plannerapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public ResponseEntity<List<User>> findAll(){
        return ResponseEntity.ok(userRepository.findAll());
    }

    public ResponseEntity<User> findByUsername(String username){
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username" + username));

        return ResponseEntity.ok(user);
    }

    public ResponseEntity<MessageResponse> deleteByUsername(String username){
            userRepository.deleteByUsername(username);
            return ResponseEntity.ok(new MessageResponse("User deleted successfully"));
    }

}
