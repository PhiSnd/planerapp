package com.sonderp.plannerapp.services;


import com.sonderp.plannerapp.exception.Exception.ResourceNotFoundException;
import com.sonderp.plannerapp.model.Task;
import com.sonderp.plannerapp.model.Team;
import com.sonderp.plannerapp.model.User;
import com.sonderp.plannerapp.payload.response.MessageResponse;
import com.sonderp.plannerapp.repository.TaskRepository;
import com.sonderp.plannerapp.repository.TeamRepository;
import com.sonderp.plannerapp.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Set;

@Service
@Transactional
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TeamRepository teamRepository;

    public ResponseEntity<Task> addToUser(Task task){

        User currentUser = getCurrentUser();
        task.setCreated(LocalDate.now());
        currentUser.addTask(task);
        task.setDone(false);

        taskRepository.save(task);
        userRepository.save(currentUser);

        return ResponseEntity.status(HttpStatus.CREATED).body(task);
    }

    public ResponseEntity<Task> addToTeam(Long teamId ,Task task){
        Team team = teamRepository.findById(teamId)
                                .orElseThrow(() -> new ResourceNotFoundException("Team doesn't exist"));

        task.setCreated(LocalDate.now());
        task.setDone(false);
        team.addTask(task);

        taskRepository.save(task);
        teamRepository.save(team);

        return ResponseEntity.status(HttpStatus.CREATED).body(task);
    }

    public ResponseEntity<Task> getTaskById(Long id){
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(""));

        return ResponseEntity.ok(task);
    }


    //Retrieves the task set by calling the userRepositry and get the tasks from the user-object (Parent -> Child(multiple))
    public ResponseEntity<Set<Task>> findAllTasksByUsername(String username){
       User user = userRepository.findByUsername(username)
               .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username " + username));

       Set<Task> tasks = user.getTasks();

       return ResponseEntity.ok(tasks);
    }


    //Retrieves the task set by calling the TaskRepository using method ' findAllByTeamId '
    public ResponseEntity<Set<Task>> findAllTasksByTeamId(Long teamId){

        boolean teamExists = teamRepository.existsById(teamId);
        if (teamExists == false){
            throw new ResourceNotFoundException("This Team doesn't exist yet...");
        }

        Set<Task> tasks = taskRepository.findAllByTeamId(teamId);
        return ResponseEntity.ok(tasks);
    }

    public ResponseEntity<Set<Task>> findAllTasksForCurrentUser(){
        User currentUser = getCurrentUser();

        return ResponseEntity.ok(taskRepository.findAllByUserId(currentUser.getId()));
    }

    public ResponseEntity<MessageResponse> deleteById(Long id){
        taskRepository.deleteTaskById(id);
        return ResponseEntity.ok(new MessageResponse("Task deleted successfully"));

    }


    private User getCurrentUser(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = userDetails.getUsername();
        User currentUser = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Error: User not found"));

        return currentUser;
    }

}
